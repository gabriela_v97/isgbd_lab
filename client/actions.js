$(document).ready(function(){
	var columns = [];
	var pk = [];
	var length_columns_array = 0;
	var checkedIsNull = false;
	var checkedIsPK = false;
	var checkedIsFK=false;

	
	var rows = document.getElementsByTagName("tr");
    for (var i = 0; i < rows.length; i++)
    {
        rows[i].onclick = function() {
           console.log(this);
        };
    }
	
	$("#btnDBs").click(function(){
		if($('.showDBs').length==0){
			$.ajax({
				url: "http://localhost:8080/dbs/getDbs",   
				type: "get",
				success: function (response) {
					// console.log(response.length);   
					let dbs="<ul class='showDBs show'>";
					for (var i=0;i<response.length;i++) {
						dbs+="<li id='btn"+response[i]+"'><input type='button' class='btn btn-secondary' value='"+response[i]+"' onclick='showTables(&quot;"+response[i]+"&quot;)'>"+
						" <button type='button' class='btn btn-link' style='color:red' onclick='deleteDatabase(&quot;"+response[i]+"&quot;)'>Delete</button>  "
						+
						"<button id='btnClick' type='button' class='btn btn-link' onclick='showFormAddTable(&quot;"+response[i]+"&quot;)'>Add table</button></li>";
					}  
					dbs+="</ul>";
					$("#liDBs").append(dbs);  
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}	
			});
		}
		else{
			if($(".showDBs").hasClass("show")){
				$(".showDBs").removeClass("show");
				$(".showDBs").addClass("hide");
			}else{
				$(".showDBs").removeClass("hide");
				$(".showDBs").addClass("show");
			}
		}
	});

	$("#btnShowFormAddDb").click(function(){
		$(".form.show").removeClass("show");
		$(".form").addClass("hide");
		$("#formAddDb").removeClass("hide");
		$("#formAddDb").addClass("show");

	});

	$("#btnCreateDB").click(function(){
		var input=$("#inputDB").val();
		console.log(input);
		if(input){
			if(input.indexOf(" ")<0){
				$.ajax({
					url: "http://localhost:8080/dbs",   
					type: "post",
					data: {"name":input} ,
					success: function (response) {

						if($('.showDBs').length!=0){
							$(".showDBs").append("<li id='btn"+input+"'><input type='button' class='btn btn-secondary' value='"+input+"' onclick='showTables(&quot;"+input+"&quot;)'>"+
								" <button type='button' class='btn btn-link' style='color:red' onclick='deleteDatabase(&quot;"+input+"&quot;)'>Delete</button>"
								+
								"<button id='btnClick' type='button' class='btn btn-link' onclick='showFormAddTable(&quot;"+input+"&quot;)'>Add table</button></li>");
						}
						console.log(response);  
						$("#successMessage").html("Success");
						$("#successMessage").removeClass("hide");
						$("#formAddDb").removeClass("show");   
						$("#formAddDb").addClass("hide");   
						$("#inputDB").val("");
						setTimeout(function(){ 
							$("#successMessage").addClass("hide"); }, 2000);     
					},
					error: function(jqXHR, textStatus, errorThrown) {
						// error: function(response) {
							// console.log
							$("#successMessage").html("This database already exist!");
							$("#successMessage").removeClass("hide");
							$("#successMessage").removeClass("btn-success");
							$("#successMessage").addClass("btn-danger");
							setTimeout(function(){ 
								$("#successMessage").addClass("hide");
								$("#successMessage").removeClass("btn-danger");
								$("#successMessage").addClass("btn-success");
							}, 2000);     
						// console.log(textStatus, errorThrown);
					}
				});
			}else{
				$("#successMessage").html("Database name should not contain spaces!");
				$("#successMessage").removeClass("hide");
				$("#successMessage").removeClass("btn-success");
				$("#successMessage").addClass("btn-danger");
				setTimeout(function(){ 
					$("#successMessage").addClass("hide");
					$("#successMessage").removeClass("btn-danger");
					$("#successMessage").addClass("btn-success");
				}, 2000);     
			}
		}else{
			$("#successMessage").html("Database is empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function(){ 
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);     
		}
	});

	$("#btnAddTable").click(function(){ 
		var databaseName = $("#insertInDb").html();
		
		var tableName = $("#inputTableName").val();
		var rowLength = $("#inputTableRowLength").val();

		if(tableName &&rowLength &&columns.length>0){
			$.ajax({
				url: "http://127.0.0.1:8080/dbs/addTable", 
				type: "POST",
				data: {"database":databaseName,
				"table":tableName,
				"rowLength" : rowLength,
				"fields" : columns.toString(),
				"foreignKeys":pk.toString()},
				success: function (response) {
					$("#inputTableName").val("");
					$("#inputTableRowLength").val("");
					$("#fieldsAdded").html("");
					emptyColumns();
					columns=[];
					pk=[]
					//adauga tabel la lista de tabele
					let className=".tables"+databaseName;
					if($(className).length!=0){
						let table="<li id='btn"+tableName+databaseName+"' ><input class='btn btn-info' type='button' value='"+tableName+"' onclick='showColumns(&quot;"+tableName+"&quot;)'>"+
						"<button type='button' class='btn btn-link' style='color:red' onclick='deleteTable(&quot;"+tableName+"&quot;,&quot;"+databaseName+"&quot;)'>Delete</button> "		
						+
						"<button type='button' class='btn btn-link' style='color:red' onclick='showIndexForm(&quot;"+tableName+"&quot;,&quot;"+databaseName+"&quot;)'>Add index</button> "
						+
						"<button type='button' class='btn btn-link' style='color:red' onclick='addRecord(&quot;"+tableName+"&quot;,&quot;"+databaseName+"&quot;)'>Add new record</button> "
						+"</li>";
						$(className).append(table);
					}
					$(".form.show").removeClass("show");
					$("#formAddTable").addClass("hide");

					$("#successMessage").html("Success");
					$("#successMessage").removeClass("hide");
					$("#formAddDb").removeClass("show");   
					$("#formAddDb").addClass("hide");   
					$("#inputDB").val("");
					setTimeout(function(){ 
						$("#successMessage").addClass("hide"); }, 3000);     
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
					columns=[];
					pk=[];
					$("#successMessage").html("Database already contains this table name! ");
					$("#successMessage").removeClass("hide");
					$("#successMessage").removeClass("btn-success");
					$("#successMessage").addClass("btn-danger");
					setTimeout(function(){ 
						$("#successMessage").addClass("hide");
						$("#successMessage").removeClass("btn-danger");
						$("#successMessage").addClass("btn-success");
					}, 3000);   
				}

			});
		}else{
			$("#successMessage").html("Fields should not be empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function(){ 
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);     

		}
	});

	$("#btnAddMoreColumns").click(function(){ 

		var name =$("#inputTableDetailsName").val();
		var type =$("#inputTableDetailsType").val();
		var length =$("#inputTableDetailsLength").val();
		
		checkedIsNull = $('#inputTableDetailsIsNull').is(':checked');
		checkedIsPK = $('#inputTableDetailsPK').is(':checked');
		checkedIsFK=$("#inputTableDetailsFK").is(':checked');
		//adauga coloanele in vector
		if(name && type && length){

			if(checkedIsFK==true){

				let tabelFk=$("#selectTable").val();
				let fieldFk=$("#inputField").val();
				if(tabelFk && fieldFk){
					columns.push(name);
					columns.push(type);
					columns.push(length);

					if(checkedIsNull == false)
						columns.push(0);
					else
						columns.push(1);
					if(checkedIsPK == false)
						columns.push(0);
					else
						columns.push(1);
					pk.push(name);
					pk.push(tabelFk);
					pk.push(fieldFk);
					$("#inputField").val("");
					$("#fieldsAdded").append("Name: "+name+", Type: "  +type +", Length: "+length+", isNull: "+checkedIsNull+", isPrimaryKey: "+checkedIsFK+", isForeignKey: "+checkedIsFK);
					emptyColumns();
					$("#formFK").removeClass("show");
					$("#formFK").addClass("hide");

				}else{
					$("#successMessage").html("Fields for Fk should not be empty!");
					$("#successMessage").removeClass("hide");
					$("#successMessage").removeClass("btn-success");
					$("#successMessage").addClass("btn-danger");
					setTimeout(function(){ 
						$("#successMessage").addClass("hide");
						$("#successMessage").removeClass("btn-danger");
						$("#successMessage").addClass("btn-success");
					}, 3000);     
				}

			}else{
				columns.push(name);
				columns.push(type);
				columns.push(length);

				if(checkedIsNull == false)
					columns.push(0);
				else
					columns.push(1);
				if(checkedIsPK == false)
					columns.push(0);
				else
					columns.push(1);
				$("#fieldsAdded").append("Name: "+name+", Type: "  +type +", Length: "+length+", isNull: "+checkedIsNull+", isPrimaryKey: "+checkedIsPK+", isForeignKey: "+checkedIsFK);
				emptyColumns();
			}
		}else{
			$("#successMessage").html("Fields should not be empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function(){ 
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);     

		}
	});
	$("#btnMultipleSelect").click(function(){
		let selectedItems=$("#selectFields").val();
		let indexName=$("#inputIndexName").val();
		let databaseName=$("#createIndexInDatabase").html();
		let tableName=$("#createIndexInTable").html();
		// for (var i =0;i< selectedItems.length; i++) {
		// 	console.log(selectedItems[i]);			
		// }
		if(selectedItems.length>0 &&indexName){
			$.ajax({
				url: "http://127.0.0.1:8080/dbs/addIndex", 
				type: "POST",
				data: {"database":databaseName,
				"table":tableName,
				"indexName" : indexName,
				"fields" : selectedItems.toString()},
				
				success: function (response) {
					$("#inputIndexName").val(""); 
					$(".form.show").removeClass("show");
					$("#formIndex").addClass("hide");

					$("#successMessage").html("Success");
					$("#successMessage").removeClass("hide");
					$("#formAddDb").removeClass("show");   
					$("#formAddDb").addClass("hide");   
					$("#inputDB").val("");
					setTimeout(function(){ 
						$("#successMessage").addClass("hide"); }, 2000);     
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}

			});
		} else{
			$("#successMessage").html("Plese add a name and select at least one column! ");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function(){ 
				$("#successMessage").addClass("hide");
				$("#successMessage").rmoveClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);   
		}

	})
});

function emptyColumns(){
	$("#inputTableDetailsName").val("");
	$("#inputTableDetailsType").val("");
	$("#inputTableDetailsLength").val("");
	$('input[type=checkbox]').prop('checked',false);
}

function showTables(dbName){
	let className=".tables"+dbName;
	if($(className).length==0){
		$.ajax({
			url: "http://localhost:8080/dbs/getTables",   
			type: "post",
			data: {database:dbName},
			success: function (response) {
				// console.log(response);   
				let dbs="<ul class='tables"+dbName+" show'>";
				for (var i=0;i<response.length;i++) {
					dbs+="<li id='btn"+response[i]+dbName+"' ><input class='btn btn-info' type='button' value='"+response[i]+"' onclick='showColumns(&quot;"+dbName+";"+response[i]+"&quot;)'>"+
					"<button type='button' class='btn btn-link' style='color:red' onclick='deleteTable(&quot;"+response[i]+"&quot;,&quot;"+dbName+"&quot;)'>Delete</button> "		
					+
					"<button type='button' class='btn btn-link' style='color:green' onclick='showIndexForm(&quot;"+response[i]+"&quot;,&quot;"+dbName+"&quot;)'>Add index</button> "
					+"</li>";
				}  
				dbs+="</ul>";
				let btn="#btn"+dbName;
				$(btn).append(dbs);  
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}	
		});
	}
	else{
		if($(className).hasClass("show")){
			$(className).removeClass("show");
			$(className).addClass("hide");
		}else{
			$(className).removeClass("hide");
			$(className).addClass("show");
		}
	}
}

function showColumns(name){
	//alert(name);
	var str = name.split(";");
	$.ajax({
		url: "http://localhost:8080/dbs/getFields",
		type: "POST",
		data: {
			"database" : str[0],
			"table" : str[1]
		},
		success: function (response) {
			var elem = "";
			document.write("<br> <br> <br>");
			document.write(" <center> <h1> Table: "+ str[1]+"</h1> </center>");
			document.write("<br> <br> <br>");
			//document.write("<button onclick='goBack()'>Go Back</button>")
			document.write(" <center> <h2> Constraints: </h2>  </center>");
			document.write("<center> <button onclick='getConstraints(&quot;"+str[0]+"-"+str[1]+"&quot;)'> Get constraints </button> </center>");
			document.write("<br> <br> <br>");
			/*document.write( "<center>  SELECT   ");
			document.write("<input id ='selectType'> </input>");
			//document.write(" <center> <table border='1' width='200' id = 'idTableShowSelect'> </tabel> </center>"); 
			document.write("FROM " + str[1] + "   " + "WHERE   ");
			document.write("<select id='mySelectConstraints'> </select>");
			document.write("  ");
			document.write("<select id='mySelectOperators'> </select>");
			$("#mySelectOperators").append("<option value='"+"=="+"'>"+"=="+"</option>");
			$("#mySelectOperators").append("<option value='"+"!="+"'>"+"!="+"</option>");
			$("#mySelectOperators").append("<option value='"+"<="+"'>"+"<="+"</option>");
			$("#mySelectOperators").append("<option value='"+">="+"'>"+">="+"</option>");
			$("#mySelectOperators").append("<option value='"+">"+"'>"+">"+"</option>");
			$("#mySelectOperators").append("<option value='"+"<"+"'>"+"<"+"</option>");
			document.write("  ");
			document.write("<input id ='selectValue'> </input> </center> ");
			document.write("<br>");
			document.write("<center> <button onclick='getSelectResults(&quot;"+str[0]+"-"+str[1]+"&quot;)'> Get records </button> </center>");
			document.write("<p id='showSelect'> </p>");
			document.write("<br> <br> <br>");
			*/
			document.write(" <center> <h2> Add new record: </h2> </center>")
			document.write(" <center> <table border='1' width='200'> </center>")
			for (var i=0;i<response.length;i++) {
				var idVal = "idInput"+response[i];
				document.write("<center> <tr>" + "</td><td>" + response[i] + "</td><td>  <input id ="+ idVal+"> </input> </td></tr> </center>");
				elem += response[i] + ";";
			}  
			document.write("</table>");
			document.write("<br> <br>");
			document.write("<button onclick='addNewRecord(&quot;"+str[0]+"-"+str[1]+"-"+elem+"&quot;)'> Add new record </button>");
			document.write("<br> <br> <br>");
			document.write(" <center> <h2> Delete record: </h2> </center>")
			document.write("<button  onclick='deleteRecord(&quot;"+str[0]+"-"+str[1]+"&quot;)'> Delete record </button>");
			document.write("<select id='mySelect'> </select>");	
			document.write("<br> <br> <br>");
			document.write(" <center> <h2> Show all records: </h2> </center>")
			document.write("<button  onclick='getAllRecords(&quot;"+str[0]+"-"+str[1]+"-"+elem+"&quot;)'> Show records </button>");
			document.write("<br> <br> <br>");
			document.write(" <center> <h2> GROUP BY and HAVING: </h2> </center>");
			document.write("<br> <br> <br>");
			document.write( "<center>  SELECT   ");
			document.write("<select id='mySelectAggregateGroupBY'> </select>");
			$("#mySelectAggregateGroupBY").append("<option value='"+"COUNT"+"'>"+"COUNT"+"</option>");
			$("#mySelectAggregateGroupBY").append("<option value='"+"AVG"+"'>"+"AVG"+"</option>");
			$("#mySelectAggregateGroupBY").append("<option value='"+"SUM"+"'>"+"SUM"+"</option>");
			$("#mySelectAggregateGroupBY").append("<option value='"+"MAX"+"'>"+"MAX"+"</option>");
			$("#mySelectAggregateGroupBY").append("<option value='"+"MIN"+"'>"+"MIN"+"</option>");
			document.write("<select id='inputAggregateValue'> </select>");
			document.write("<input id ='selectTypeGroupBY'> </input>");
			document.write("<br> <br>");
			document.write("FROM " + str[1]);
			document.write("<br> <br>");
			document.write("GROUP BY " + "<select id ='inpuSelectedValueForGroupBY'> </select>");
			document.write("<br> <br>");
			document.write("HAVING " + "<input id ='inpuSelectedValueForHaving'> </input>");
			document.write("<select id='mySelectHavingOperators'> </select>");
			document.write("  ");
			$("#mySelectHavingOperators").append("<option value='"+"=="+"'>"+"=="+"</option>");
			$("#mySelectHavingOperators").append("<option value='"+"!="+"'>"+"!="+"</option>");
			$("#mySelectHavingOperators").append("<option value='"+"<="+"'>"+"<="+"</option>");
			$("#mySelectHavingOperators").append("<option value='"+">="+"'>"+">="+"</option>");
			$("#mySelectHavingOperators").append("<option value='"+">"+"'>"+">"+"</option>");
			$("#mySelectHavingOperators").append("<option value='"+"<"+"'>"+"<"+"</option>");
			document.write("  ");
			document.write("<input id ='inputHavingValue'> </input> </center>");
			document.write("<br> <br>");
			document.write("<button  onclick='getGroupByRecords(&quot;"+str[0]+"-"+str[1] +"&quot;)'> Show records </button>");
			
			
			document.write("<br> <br> <br> <br> <br> <br>");
			document.write("<center> <h2> SELECT </h2> </center>");
			document.write( "<center>  SELECT:   ");
			document.write("<input id ='selectType'> </input>");
			//document.write(" <center> <table border='1' width='200' id = 'idTableShowSelect'> </tabel> </center>"); 
			document.write("FROM " + str[1] + "   " + "WHERE   ");
			document.write("<select id='mySelectConstraints'> </select>");
			document.write("  ");
			document.write("<select id='mySelectOperators'> </select>");
			$("#mySelectOperators").append("<option value='"+"=="+"'>"+"=="+"</option>");
			$("#mySelectOperators").append("<option value='"+"!="+"'>"+"!="+"</option>");
			$("#mySelectOperators").append("<option value='"+"<="+"'>"+"<="+"</option>");
			$("#mySelectOperators").append("<option value='"+">="+"'>"+">="+"</option>");
			$("#mySelectOperators").append("<option value='"+">"+"'>"+">"+"</option>");
			$("#mySelectOperators").append("<option value='"+"<"+"'>"+"<"+"</option>");
			document.write("  ");
			document.write("<input id ='selectValue'> </input> </center> ");
			document.write("<br>");
			document.write("<center> <button onclick='getSelectResults(&quot;"+str[0]+"-"+str[1]+"&quot;)'> Get records </button> </center>");
			document.write("<p id='showSelect'> </p>");
			document.write("<br> <br> <br>");
		},
		error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
		}
	});
}

function goBack() {
    window.history.back();
}

function getGroupByRecords(values){
	var selectedAggregate = document.getElementById("mySelectAggregateGroupBY").value;
	var selectedAggregateCol = document.getElementById("inputAggregateValue").value;
	var selectedTypeCol = document.getElementById("selectTypeGroupBY").value;
	var selectedGroupBy = document.getElementById("inpuSelectedValueForGroupBY").value;
	var operator = document.getElementById("mySelectHavingOperators").value;
	var value = document.getElementById("inputHavingValue").value;
//	alert("selectedAggregate = " + selectedAggregate + "  selectedAggregateCol = " + selectedAggregateCol + "  selectedTypeCol = " + selectedTypeCol + " selectedGroupBy = " + selectedGroupBy + " operator = " + operator + " value = " + value);
	var v = values.split("-");
	$.ajax({
		url: "http://localhost:8080/dbs/getGroupByResults", 
		type: "GET",
		data: {"database":v[0], 
			   "table":v[1],
			   "aggregate": selectedAggregate,
			   "columnAggregate":selectedAggregateCol,
			   "columnGroupBy":selectedGroupBy,
			   "operator":operator,
			   "value": value
			   } ,
			success: function (response) { 
				alert(response);
				document.write(" <center> <table border='1' width='200'> </center>")
				document.write("<th>" + selectedAggregate + "</th>");
				document.write("<th>" + selectedTypeCol + "</th>");
					for (var i=0;i<response.length;i++) {
						var elemente = response[i].split("#");
						document.write("<tr>" + "<td>" + elemente[0] + "</td>");
						document.write( "<td>" + elemente[1] + "</td>");
						document.write("</tr>");
						$("#mySelect").append("<option value='"+response[i]+"'>"+response[i]+"</option>");
					}  		
					document.write("</table>");	
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}	
	
});
}

function getSelectResults(values){
	var selected = document.getElementById("mySelectConstraints").value;
	var v = values.split("-");
	$.ajax({
		url: "http://localhost:8080/dbs/getSelectResults", 
		type: "GET",
		data: {"database":v[0], 
			   "table":v[1],
			   "columns": "*",
			   "field":document.getElementById("mySelectConstraints").value,
			   "operator":document.getElementById("mySelectOperators").value,
			   "value":document.getElementById("selectValue").value
			   } ,
			success: function (response) { 
				var fields = document.getElementById("selectType").value;
					if(fields != '*'){
						document.write(" <center> <table border='1' width='200'> </center>")
			//	document.write("<th>" + selectedAggregate + "</th>");
						//var paragraph = document.getElementById("showSelect");
						//document.getElementById("showSelect").innerHTML = "";
					    //var table = document.getElementById("tableSelect");
						var valuesSelected = fields.split(",");
						//alert(valuesSelected);
						//document.write("<center> <th>" + "ssss" + "</th> </center>");
						for(var i = 0 ; i < valuesSelected.length; i++){
							document.write("<center> <th>" + valuesSelected[i] + "</th> </center>");
							//document.write("<center> <th>" + "ssss" + "</th> </center>");
						}
						var val = "";
						var ff = fields.split(",");
						for(var k = 0; k<response.length;k++){
							document.write("<center> <tr>");
							var resp = response[k].split("#");
							var responses = resp[1].split(";");
							for(var i = 0 ; i < responses.length ; i++){
								
								var sp = responses[i].split(":");
								for(var j = 0 ; j < ff.length ; j++){
									ff[j].replace(/^\s+|\s+$/g, '');
									if(ff[j] == sp[0]){
										var elem = sp[0] + " : " + sp[1];
										val += elem ;
										document.write("<td>" + sp[1]+ "</td>");
										//val += ""
										//val	+= "\n";
									}
								}
								
							}
							document.write(" </tr> </center>");
						}
							//var text = document.createTextNode(val);
							//paragraph.appendChild(text);
							document.write("</table>");
					}
					else{
						document.write(" <center> <table border='1' width='200'> </center>");
						var value = "";
						//var paragraph = document.getElementById("showSelect");
						var ff = fields.split(",");
						document.write("<th>" + "Id" + "</th>");
						//for(var k = 0; k<response.length;k++){
							//document.write("<center> <tr>");
							var resp = response[0].split("#");
							//document.write("<td>" + resp[0] + "</td>");
							var values = resp[1].split(";");
							for(var i = 0 ; i < values.length-1; i++){
								var val = values[i].split(":");
								document.write("<th>" + val[0] + "</th>");
							}
							//value += "Id:" + resp[0] + ";        ";
							//value += resp[1];
							console.log(resp[1]);

						//}
						for(var k = 0; k<response.length;k++){
							document.write("<center> <tr>");
							var resp = response[k].split("#");
							document.write("<td>" + resp[0] + "</td>");
							var values = resp[1].split(";");
							for(var i = 0 ; i < values.length-1; i++){
								var val = values[i].split(":");
								document.write("<td>" + val[1] + "</td>");
							}
							//value += "Id:" + resp[0] + ";        ";
							//value += resp[1];
							console.log(resp[1]);
							document.write(" </tr> </center>");
						}
						document.write("</table>");
					//	var text = document.createTextNode(value);
						//paragraph.appendChild(text);
					}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}		
	});
}

function getConstraints(values){
	var selected = document.getElementById("mySelectConstraints").value;
	var v = values.split("-");
	$.ajax({
		url: "http://localhost:8080/dbs/getConstraints", 
		type: "GET",
		data: {"database":v[0], 
			   "table":v[1]} ,
			success: function (response) { 
				$("#mySelectConstraints").empty();
				$("#inputAggregateValue").empty();
				$("#inpuSelectedValueForGroupBY").empty();
				var elemente = response.split("#");
				for (var i=0;i<elemente.length;i++) {
					$("#mySelectConstraints").append("<option value='"+elemente[i]+"'>"+elemente[i]+"</option>");
					$("#inputAggregateValue").append("<option value='"+elemente[i]+"'>"+elemente[i]+"</option>");
					$("#inpuSelectedValueForGroupBY").append("<option value='"+elemente[i]+"'>"+elemente[i]+"</option>");
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}		
	});
}

function deleteRecord(values){
	var selected = document.getElementById("mySelect").value;
	var v = values.split("-");
	var elem = selected.split("#");
	var id = elem[0];
		$.ajax({
				url: "http://localhost:8080/dbs/deleteRecord",
				type: "POST",
				data: {"database":v[0],
						"table": v[1],
						"key":id} ,
				success: function (response) { 
						console.log("OK");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}
		});
}

function getAllRecords(values){
	var elem = values.split("-");	
	//for(var k = 0 ; k < $("#recordsTABLE").length; k++)
		//$("#recordsTABLE").deleteRow(k);
		//$("#recordsTABLE").deleteRow(k);
	$.ajax({
				url: "http://localhost:8080/dbs/getAllRecords",
				type: "GET",
				data: {"database":elem[0], 
						"table":elem[1]} ,
				success: function (response) { 
					$("#mySelect").empty();
					document.write(" <center> <table border='1' width='200'> </center>")
					var a = elem[2].split(";");
						//alert("leng = " + a.length + "elem = " + elem[2]);
					for(var j = 0 ; j < a.length -1 ; j++)
						document.write("<th>" + a[j] + "</th>");
					for (var i=0;i<response.length;i++) {
						var elemente = response[i].split("#");
						var ele = elemente[1].split(";");
						document.write("<tr>" + "<td>" + elemente[0] + "</td>");
						for(var k = 0 ; k < ele.length-1; k++){
					//	document.write("<center> <tr>" + "<td>" + elemente[0] + "</td><td>" + ele[0].split(":")[1] + "</td><td>" + ele[1].split(":")[1] + "</td></tr> </center>");
							document.write("<td>" + ele[k].split(":")[1] + "</td>");
						}
						document.write("</tr>");
						$("#mySelect").append("<option value='"+response[i]+"'>"+response[i]+"</option>");
					}  		
					document.write("</table>");	
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}
			});
}

function addNewRecord(values){
	var e1 = values.split("-");
	var elem = e1[2].split(";");
	var value = "";
	for(var i = 1; i < elem.length-1; i++){
		var id = "idInput" + elem[i];
		value += elem[i]+":"+document.getElementById(id).value+";";
	}
	var idRecord = "idInput" + elem[0];
	$.ajax({
				url: "http://localhost:8080/dbs/addRecord",
				type: "POST",
				data: {"database":e1[0],
						"table": e1[1],
						"key": document.getElementById(idRecord).value,
						"value":value} ,
				success: function (response) { 
						console.log(response);
						if(response == "duplicateKey"){
							alert("Duplicate key");
						}else{
							if(response == "succes"){
							alert("Add record");
							}else{
							if(response == "error: duplicate uk!"){
								alert("Duplicate uk!");
							}
							else{
								alert("Doesn't exist this FK");
							}
							}
						}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}
			});
}

function deleteDatabase(dbName){
	if (confirm("Are you sure?")) {
		$.ajax({
			url: "http://localhost:8080/dbs/deleteDb",
			type: "POST",
			data: {"name":dbName} ,
			success: function (response) {
				let line="#btn"+dbName;
				$(line).remove();      
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
     
    }
    return false;
}

function deleteTable(tableName,dbName){
	if (confirm("Are you sure ?")) {
		$.ajax({
			url: "http://localhost:8080/dbs/deleteTable",
			type: "POST",
			data: {"tableName":tableName,
			"dbName":dbName} ,
			success: function (response) {
				let line="#btn"+tableName+dbName;
				$(line).remove();
				console.log(response);          
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
    }
    return false;
}

function showFormFK(){
	if($("#formFK").hasClass("show")){
		$("#formFK").removeClass("show");
		$("#formFK").addClass("hide");
	} else{


		$("#formFK").removeClass("hide");
		$("#formFK").addClass("show");
		let db=$("#insertInDb").html();
		$.ajax({
			url: "http://localhost:8080/dbs/getTables",   
			type: "post",
			data: {database:db},
			success: function (response) {
				// console.log(response);   
				$("#selectTable").empty();
				for (var i=0;i<response.length;i++) {
					$("#selectTable").append("<option value='"+response[i]+"'>"+response[i]+"</option>");
				}  

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}	
		});
	}
}

function addRecord(tabel, database){}

function showFormAddTable(database){
	$("#insertInDb").html(database);
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formAddTable").removeClass("hide");
	$("#formAddTable").addClass("show");
}

function showIndexForm(tableName,dbName){
	$("#createIndexInDatabase").html(dbName);
	$("#createIndexInTable").html(tableName);
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formIndex").removeClass("hide");
	$("#formIndex").addClass("show");
	$.ajax({
		url: "http://localhost:8080/dbs/getFields",   
		type: "post",
		data: {database:dbName,
			table:tableName},
			success: function (response) {  
				$("#selectFields").empty();
				for (var i=0;i<response.length;i++) {
					$("#selectFields").append("<option value='"+response[i]+"'>"+response[i]+"</option>");
				}  

			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}	
		});
}