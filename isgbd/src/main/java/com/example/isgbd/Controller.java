package com.example.isgbd;

import com.example.isgbd.Berkeley.IndexService;
import com.example.isgbd.Berkeley.Service1;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

import java.util.List;

@RestController
@RequestMapping("/dbs")
public class Controller {
    public DOMParser domParser;

    public Controller() {
        domParser = new DOMParser();
    }

    //incercari de adaugare
    public static void workWithXML() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("company");
            doc.appendChild(rootElement);

            Element studenti = doc.createElement("studenti");
            rootElement.appendChild(studenti);

            Element student1 = doc.createElement("student");
            studenti.appendChild(student1);

            Element firstname1 = doc.createElement("firstname");
            firstname1.appendChild(doc.createTextNode("primul"));
            student1.appendChild(firstname1);

            //
            Element student2 = doc.createElement("student");
            studenti.appendChild(student2);

            Element firstname2 = doc.createElement("firstname");
            firstname2.appendChild(doc.createTextNode("al doilea"));
            student2.appendChild(firstname2);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("C:\\Users\\Irina\\workingwithxml\\src\\file.xml"));
            transformer.transform(source, result);
            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/getDbs",method = RequestMethod.GET)
    public List<String> findAllDatabases() {
        return domParser.getDatabases();
    }

    @CrossOrigin
    @RequestMapping(value = "/getTables",method = RequestMethod.POST)
    public  List<String> getTables(@RequestParam String database) {
        return domParser.getTables(database);
    }

    @CrossOrigin
    @RequestMapping(value = "/getFields",method = RequestMethod.POST)
    public  List<String> getTables(@RequestParam String database,@RequestParam String table) {
        return domParser.getFields(database,table);
    }


    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addDb(@RequestParam String name) {
//        System.out.println(name);

        if (!domParser.checkDb(name)) {
            domParser.createDB(name);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("This database already exist!", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteDb", method = RequestMethod.POST)
    public ResponseEntity<?> deleteDB(@RequestParam String name) {
        System.out.println(name);
        if (!name.isEmpty()) {
            domParser.deleteDB(name);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteTable", method = RequestMethod.POST)
    public ResponseEntity<?> deleteTable(@RequestParam String tableName,@RequestParam String dbName) {
//        System.out.println(name);
        if (!tableName.isEmpty()&&!dbName.isEmpty()) {
            domParser.deleteTable(dbName,tableName);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/addTable", method = RequestMethod.POST)
    public ResponseEntity<?> addTable(@RequestParam String database,@RequestParam String table,@RequestParam String rowLength, @RequestParam List<String> fields,@RequestParam List<String> foreignKeys) {
//        System.out.println(table);
//        System.out.println(rowLength);
//        System.out.println(database);
//        System.out.println(fields);
//        System.out.println("foreign keys: "+ foreignKeys);

//        if (!database.isEmpty()&&!table.isEmpty()&&!rowLength.isEmpty()&&!fields.isEmpty()) {
         if(!domParser.checkTable(database,table)){
            domParser.addTable(database,table,rowLength,fields,foreignKeys);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }
    @CrossOrigin
    @RequestMapping(value = "/addIndex", method = RequestMethod.POST)
    public ResponseEntity<String> addIndex(@RequestParam String database,@RequestParam String table,@RequestParam String indexName, @RequestParam List<String> fields) {
        System.out.println(table);
        System.out.println(database);
        System.out.println(indexName);
        System.out.println(fields);
        if (!database.isEmpty()&&!table.isEmpty()&&!fields.isEmpty()) {
            domParser.addIndex(database,table,indexName,fields);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        }else{
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    //value = id:1;nume:ana;...
    @CrossOrigin
    @RequestMapping(value = "/addRecord",method = RequestMethod.POST)
    public ResponseEntity<String> addRecord(@RequestParam("database") String database,
                                            @RequestParam("table") String table,
                                            @RequestParam("key") String key,
                                            @RequestParam("value") String value){
        Service1 service = new Service1();
        String FK = domParser.getFK(database, table);
        System.out.println("table FK : " + FK);
        String[] fks = FK.split("#");
        FK = fks[0];
        String fkTable = "";
        if(fks.length > 2)
           fkTable = fks[2];

        Service1 service1 = new Service1();

        String UK = domParser.getUniqueKey(database, table);
        String NUK = domParser.getNonUniqueKey(database, table);
        String fkValue = null; String ukValue = null; String newValue = ""; String nuk = "";
        String[] elements = value.split(";");
        String[] values;
        for(String element : elements){
            System.out.println("element = " +element);
                values = element.split(":");
                newValue += values[1] + "#";
                System.out.println("FK = " + FK);
                System.out.println("UK = " + UK);
                System.out.println("NUK = " + NUK);
                if (values[0].equals(FK)) {
                    fkValue = values[1];
                    if(!fkTable.equals(""))
                    if(!service1.findByKey(database, fkTable, fkValue).equals("found"))
                        return new ResponseEntity<String>("FK nu exista in tabelul parinte!", HttpStatus.OK);
                        System.out.println("fk: " + values[0] + " -> " + fkValue);
                }
                if (values[0].equals(UK)) {
                    ukValue = values[1];
                    System.out.println("uk: " + values[0] + " -> " + ukValue);
                }
                //----//
                if (values[0].equals(NUK)) {
                    nuk = values[1];
                    System.out.println("nuk: " + values[0] + " -> " + nuk);
                 }
        }

        String response = service.addNewRecord(database, table, key, value, fkValue, ukValue, nuk);
        System.out.println("------------------- ALL VALUES FROM " + table +" ----------------");
        service.readAllRecords(database, table);
        IndexService indexService = new IndexService();
        System.out.println("------------------- FK index ----------------");
        indexService.readRecords(database, table, "FK");
        System.out.println("------------------- UK index ----------------");
        indexService.readRecords(database, table, "UK");
        System.out.println("------------------- NUK index ----------------");
        indexService.readRecords(database, table, "NUK");
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getAllRecords",method = RequestMethod.GET)
    public List<String> addRecord(@RequestParam("database") String database,
                                            @RequestParam("table") String table){
        Service1 service = new Service1();
        return service.readAllRecords(database, table);
        //return new ResponseEntity<String>("ok", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteRecord",method = RequestMethod.POST)
    public ResponseEntity<String> deleteRecord(@RequestParam("database") String database, @RequestParam("table") String table,
                                            @RequestParam("key") String key){
        Service1 service = new Service1();
        String resp = service.deleteRecord(database, table, key);
        System.out.println("RESPONSE FROM DELETE: "+ resp);
        return new ResponseEntity<String>(resp, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/addIndexRec",method = RequestMethod.POST)
    public ResponseEntity<String> addIndexFK(@RequestParam("database") String database,
                                             @RequestParam("table") String table,
                                             @RequestParam("key") String key){
        IndexService index = new IndexService();
        //if(key.equals("FK")){
           // index.addNewIndexFK(database, table);
         //   index.readRecords(database, table, "");
       // }
        return new ResponseEntity<String>("ok", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/searchByFK",method = RequestMethod.GET)
    public ResponseEntity<String> searchByFK(@RequestParam("database") String database,
                                             @RequestParam("table") String table,
                                             @RequestParam("key") String key){
        IndexService index = new IndexService();
        index.findAllByFK(database, table, key);
        return new ResponseEntity<String>("ok", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getFK",method = RequestMethod.GET)
    public ResponseEntity<String> getFK(@RequestParam("database") String database,
                                             @RequestParam("table") String table){
        domParser.getFK(database, table);
        domParser.getUniqueKey(database, table);
        return new ResponseEntity<String>("ok", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/searchByPK",method = RequestMethod.GET)
    public ResponseEntity<String> searchByPK(@RequestParam("database") String database,
                                             @RequestParam("table") String table,
                                             @RequestParam("key") String key){
        Service1 service = new Service1();
        System.out.println("------- Find by PK ------");
        service.readRecords(database, table, key);
        return new ResponseEntity<String>("ok", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getConstraints",method = RequestMethod.GET)
    public ResponseEntity<String> getAllConstraints(@RequestParam("database") String database,
                                             @RequestParam("table") String table){
        System.out.println("response = " + database + "-"+table);
        String pk = domParser.getPrimaryKey(database, table);
        String fk = domParser.getFK(database, table);
        String uk = domParser.getUniqueKey(database, table);
        String nonUk = domParser.getNonUniqueKey(database, table);
        //response = pk:id#fk:id1#uk:id2
        String response = "PK:"+pk; //+"#fk:"+fk+"#uk:"+uk;
        if(!fk.equals("")) {
            String[] f = fk.split("#");
            response += "#FK:" + f[0];
        }
        if(!uk.equals(""))
            response += "#UK:"+uk;
        if(!nonUk.equals("")){
            response += "#NUK:"+nonUk;
        }
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getSelectResults",method = RequestMethod.GET)
    public List<String> getSelectResults(@RequestParam("database") String database,
                                                    @RequestParam("table") String table,
                                                   @RequestParam("columns") String columns,
                                                   @RequestParam("field") String field,
                                                   @RequestParam("operator") String operator,
                                                   @RequestParam("value") String value){
       // System.out.println(database + "-"+table+"-" + columns + "-" + field + "-" + operator + "-" +value);
        IndexService indexService = new IndexService();
        System.out.println("------------------------------GET RES------------------------------------");
        List<String> list = indexService.getSelectResults(database, table, columns, field, operator, value);

        for(String s : list){
            System.out.println(s);
        }

        int sem = 0;
        for(int i = 0; i< list.size(); i++){
            sem = 0;
            for(int j = 0; j < list.size();j++){
                if(list.get(i).equals(list.get(j)) && i!=j)
                    sem = 1;
            }
            if(sem == 1){
                list.remove(i);
            }
        }
        return list;
       // return new ResponseEntity<String>("ok", HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getJoinResults",method = RequestMethod.GET)
    public List<String> getJoinResults(@RequestParam("database") String database,
                                         @RequestParam("table1") String table1,
                                         @RequestParam("table2") String table2,
                                         @RequestParam("column1") String column1,
                                         @RequestParam("column2") String column2,
                                         @RequestParam("relation") String relation){
        IndexService indexService = new IndexService();
        List<String> a = indexService.getJoinResults(database, table1, table2, column1, column2, relation);
        System.out.println("-------------------  /getJoinResults --------------- ");
        int count = 0;
        //for(String aa : a){
          // System.out.println(aa);
           //for(String a1 : a){
             //  if(aa.equals(a1))
               //    a.remove(aa);
           //}
        //}
        int sem = 0;
        for(int i = 0; i< a.size(); i++){
            sem = 0;
            for(int j = 0; j < a.size();j++){
                if(a.get(i).equals(a.get(j)) && i!=j)
                    sem = 1;
            }
            if(sem == 1){
                a.remove(i);
            }
        }
        return a;
    }

    @CrossOrigin
    @RequestMapping(value = "/getHashJoinResults",method = RequestMethod.GET)
    public List<String> getHashJoinResults(@RequestParam("database") String database,
                                       @RequestParam("table1") String table1,
                                       @RequestParam("table2") String table2,
                                       @RequestParam("column1") String column1,
                                       @RequestParam("column2") String column2,
                                       @RequestParam("relation") String relation){
        IndexService indexService = new IndexService();
        List<String> a = indexService.getJoinResults(database, table1, table2, column1, column2, relation);
        System.out.println("-------------------  /getHashJoinResults --------------- ");
        int sem = 0;
        for(int i = 0; i< a.size(); i++){
            sem = 0;
            for(int j = 0; j < a.size();j++){
                if(a.get(i).equals(a.get(j)) && i!=j)
                    sem = 1;
            }
            if(sem == 1){
                a.remove(i);
            }
        }
        return a;
    }

    @CrossOrigin
    @RequestMapping(value = "/getGroupByResults",method = RequestMethod.GET)
    public List<String> getGroupByResults(@RequestParam("database") String database,
                                       @RequestParam("table") String table,
                                       @RequestParam("aggregate") String aggregate,
                                       @RequestParam("columnAggregate") String columnAggregate,
                                       @RequestParam("columnGroupBy") String columnGroupBy,
                                       @RequestParam("operator") String operator,
                                       @RequestParam("value") String value) {
        IndexService indexService = new IndexService();
        System.out.println("-------------------  /getGroupByResults --------------- ");
        List<String> list = indexService.getGroupByResults(database, table, aggregate,
                columnAggregate, columnGroupBy, operator, value);
        return list;
    }
}
