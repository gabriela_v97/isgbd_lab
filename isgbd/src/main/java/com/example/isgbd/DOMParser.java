package com.example.isgbd;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOMParser {

    final String url = "C:\\Facultate\\MASTER\\Semestrul I\\ISGBD\\isgbd\\isgbd\\src\\main\\resources\\date.xml";

    public DOMParser() {
    }

    public List<String> getDatabases(){
        List<String> databases=new ArrayList<>() ;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList dbs = doc.getElementsByTagName("DataBase");
            for (int i = 0; i < dbs.getLength(); i++) {
                Node node = dbs.item(i);
                Element e = (Element) node;
                databases.add(e.getAttribute("dataBaseName"));
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return databases;
    }
    public List<String> getTables(String database){
        List<String> getTables=new ArrayList<>() ;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tableslist = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tableslist.getLength(); i++) {
                if(tableslist.item(i).getParentNode().getAttributes().getNamedItem("dataBaseName").getNodeValue().equals(database)){
                    NodeList  tables= tableslist.item(i).getChildNodes();
                    for (int j = 0; j < tables.getLength(); j++) {
                        Node table = tables.item(j);
                        if (table.getNodeType() == Node.ELEMENT_NODE) {
                            getTables.add(table.getAttributes().getNamedItem("tableName").getNodeValue());
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return getTables;
    }
    public void deleteTable(String database,String name) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tables = doc.getElementsByTagName("Tables");
            System.out.println(tables.getLength());
            for(int i=0;i<tables.getLength();i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    System.out.println("copii" + tabele.getLength());
                    for(int j=0;j<tabele.getLength();j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            System.out.println(tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue());
                            if(tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)){
                                tables.item(i).removeChild(tabele.item(j));
                            }
                        }
                    }
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File(url));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public void addTable(String database,String nume, String rowlength, List<String> campuri,List<String> fk) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList dbs = doc.getElementsByTagName("Tables");
            System.out.println(dbs.getLength());
            for (int i = 0; i < dbs.getLength(); i++) {
                Node node = dbs.item(i);
                Element e = (Element) node;
                Node parent = e.getParentNode();
                Element parinte = (Element) parent;
                if (parinte.getAttribute("dataBaseName").equals(database)) {
                    Element el = doc.createElement("Table");
                    el.setAttribute("tableName", nume);
                    el.setAttribute("fileName", nume + ".kv");
                    el.setAttribute("rowLength", rowlength);

                    Element structure = doc.createElement("Structure");
                    for (int k = 0; k < campuri.size(); k += 5) {
                        Element atribut = doc.createElement("Attribute");
                        atribut.setAttribute("attributeName", campuri.get(k));
                        atribut.setAttribute("type", campuri.get(k + 1));
                        atribut.setAttribute("length", campuri.get(k + 2));
                        atribut.setAttribute("isNull", campuri.get(k + 3));
                        structure.appendChild(atribut);
                    }
                    el.appendChild(structure);
                    Element primaryKey = doc.createElement("primaryKey");
                    List<String> pKeys=new ArrayList<>();
                    for (int k = 0; k < campuri.size(); k += 5) {
                        if (campuri.get(k + 4).equals("1")) {
                            Element pk = doc.createElement("pkAttribute");
                            pKeys.add(campuri.get(k));
                            pk.appendChild(doc.createTextNode(campuri.get(k)));
                            primaryKey.appendChild(pk);
                        }
                    }
                    el.appendChild(primaryKey);

                    Element foreignKeys = doc.createElement("foreignKeys");
                    for (int k = 0; k < fk.size(); k += 3) {
                            Element foreignKey=doc.createElement("foreignKey");
                            Element fkAttribute = doc.createElement("fkAttribute");
                            fkAttribute.appendChild(doc.createTextNode(fk.get(k)));
                            foreignKey.appendChild(fkAttribute);
                            Element references=doc.createElement("references");
                            Element refTable=doc.createElement("refTable");
                            refTable.appendChild(doc.createTextNode(fk.get(k+1)));
                            Element refAttribute=doc.createElement("refAttribute");
                            refAttribute.appendChild(doc.createTextNode(fk.get(k+2)));
                            references.appendChild(refTable);
                            references.appendChild(refAttribute);
                            foreignKey.appendChild(references);

                        foreignKeys.appendChild(foreignKey);
                    }
                    el.appendChild(foreignKeys);


                    Element indexFiles = doc.createElement("IndexFiles");
                    Element indexFile=doc.createElement("IndexFile");
                    indexFile.setAttribute("keyLength","25");
                    indexFile.setAttribute("isUnique","0");
                    indexFile.setAttribute("indexType","BTree");
                    indexFile.setAttribute("indexName",nume+".ind");
                    Element indexAttributes=doc.createElement("IndexAttributes");

                    System.out.println(pKeys);
                    for (String pk:pKeys
                         ) {
                        Element iAttribute=doc.createElement("IAttribute");
                        iAttribute.appendChild(doc.createTextNode(pk));
                        indexAttributes.appendChild(iAttribute);
                    }
                    indexFile.appendChild(indexAttributes);
                    indexFiles.appendChild(indexFile);
                    el.appendChild(indexFiles);

                    e.appendChild(el); //e este tables
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File(url));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public void createDB(String nume) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            Element rootElement = doc.getDocumentElement();

            Element db = doc.createElement("DataBase");
            db.setAttribute("dataBaseName", nume);

            Element n = doc.createElement("Tables");
            db.appendChild(n);
            rootElement.appendChild(db);

            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File(url));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public void deleteDB(String nume) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            Element rootElement = doc.getDocumentElement();
            NodeList dbs = doc.getElementsByTagName("DataBase");
            for (int i = 0; i < dbs.getLength(); i++) {
                Node node = dbs.item(i);
                Element e = (Element) node;
                String numedb = e.getAttribute("dataBaseName");
                if (numedb.equals(nume)) {
                    rootElement.removeChild(node);
                    System.out.println("Sters");
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File(url));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public boolean checkDb(String database){
        List<String> dbs=getDatabases();
        if(dbs.contains(database))
            return true;
        return false;
    }
    public boolean checkTable(String database,String table){
        List<String> tables=getTables(database);
        if(tables.contains(table))
            return true;
        return false;
    }

    public String getFK(String database,String name){
        //fk = fkName + database + table + field
        String fk = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs=tabele.item(j).getChildNodes();
                                for(int k=0;k<childs.getLength();k++){
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                    //    System.out.println(c.getNodeName());
                                        if(c.getNodeName().equals("foreignKeys")){
                                            System.out.println("---------in foreignKeys-------");
                                            NodeList attributes=c.getChildNodes();
                                            for(int x=0;x<attributes.getLength();x++){
                                                Node a=attributes.item(x);
                                                if(a.getNodeType()==Node.ELEMENT_NODE && a.getNodeName().equals("foreignKey")){
                                                    NodeList atts=a.getChildNodes();
                                                   // System.out.println(" fk------"+atts.getLength());
                                                    for(int y=0;y<atts.getLength();y++) {
                                                        Node aa = atts.item(y);
                                                        if(aa.getNodeType() == Node.ELEMENT_NODE && aa.getNodeName().equals("fkAttribute")) {
                                                            System.out.println("fk name-" + aa.getTextContent());
                                                            fk += aa.getTextContent() + "#" + database;
                                                        }
                                                        if(aa.getNodeType() == Node.ELEMENT_NODE && aa.getNodeName().equals("references")){
                                                            NodeList refs = aa.getChildNodes();
                                                            for(int l = 0 ; l < refs.getLength() ; l++){
                                                                Node r = refs.item(l);
                                                                if(r.getNodeType() == Node.ELEMENT_NODE &&
                                                                        r.getNodeName().equals("refTable")) {
                                                                    System.out.println("reference table - " + r.getTextContent());
                                                                    fk += "#" + r.getTextContent();
                                                                }
                                                                if(r.getNodeType() == Node.ELEMENT_NODE &&
                                                                        r.getNodeName().equals("refAttribute")) {
                                                                    System.out.println("reference attribute - " + r.getTextContent());
                                                                    fk += "#" + r.getTextContent();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("fk === "+ fk);
        return fk;
    }

    public String getUniqueKey(String database, String name){
        //fk = fkName + database + table + field
        String fk = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs = tabele.item(j).getChildNodes();
                                for (int k = 0; k < childs.getLength(); k++) {
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if (c.getNodeName().equals("uniqueKeys")) {
                                            System.out.println("---------in uniqueKeys-------");
                                            NodeList attributes = c.getChildNodes();
                                            for (int x = 0; x < attributes.getLength(); x++) {
                                                Node a = attributes.item(x);
                                                if (a.getNodeType() == Node.ELEMENT_NODE && a.getNodeName().equals("UniqueAttribute")) {
                                                    System.out.println("uk name-" + a.getTextContent());
                                                    fk += a.getTextContent();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("uk === "+ fk);
        return fk;
    }

    public String getNonUniqueKey(String database, String name){
        String fk = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs = tabele.item(j).getChildNodes();
                                for (int k = 0; k < childs.getLength(); k++) {
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if (c.getNodeName().equals("nonUniqueKeys")) {
                                            System.out.println("---------in nonUniqueKeys-------");
                                            NodeList attributes = c.getChildNodes();
                                            for (int x = 0; x < attributes.getLength(); x++) {
                                                Node a = attributes.item(x);
                                                if (a.getNodeType() == Node.ELEMENT_NODE && a.getNodeName().equals("NonUniqueAttribute")) {
                                                    System.out.println("nonUK name-" + a.getTextContent());
                                                    fk += a.getTextContent();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("nonUK === "+ fk);
        return fk;
    }

    public String getPrimaryKey(String database, String name){
        //fk = fkName + database + table + field
        String pk = "";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs = tabele.item(j).getChildNodes();
                                for (int k = 0; k < childs.getLength(); k++) {
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if (c.getNodeName().equals("primaryKey")) {
                                            System.out.println("---------in primaryKey-------");
                                            NodeList attributes = c.getChildNodes();
                                            for (int x = 0; x < attributes.getLength(); x++) {
                                                Node a = attributes.item(x);
                                                if (a.getNodeType() == Node.ELEMENT_NODE && a.getNodeName().equals("pkAttribute")) {
                                                    System.out.println("pk name-" + a.getTextContent());
                                                    pk += a.getTextContent();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("pk === "+ pk);
        return pk;
    }

    public List<String> getFields(String database,String name) {
        List<String> fields=new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs=tabele.item(j).getChildNodes();
                                for(int k=0;k<childs.getLength();k++){
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if(c.getNodeName().equals("Structure")){
                                            NodeList attributes=c.getChildNodes();
                                            for(int x=0;x<attributes.getLength();x++){
                                                Node a=attributes.item(x);
                                                if(a.getNodeType()==Node.ELEMENT_NODE){
                                                    fields.add(a.getAttributes().getNamedItem("attributeName").getNodeValue());
                                                    System.out.println(fields.get(fields.size()-1));

                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
            return fields;
        }

    public void addIndex(String database,String tableName,String indexName,List<String> fields) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(url);
            NodeList tables = doc.getElementsByTagName("Tables");
            System.out.println(tables.getLength());
            for(int i=0;i<tables.getLength();i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(tableName)) {
//                                tables.item(i).removeChild(tabele.item(j));
                                NodeList childs = tabele.item(j).getChildNodes();
                                for (int k = 0; k < childs.getLength(); k++) {
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if (c.getNodeName().equals("IndexFiles")) {
                                            Element indexFile=doc.createElement("IndexFile");
                                            indexFile.setAttribute("keyLength","25");
                                            indexFile.setAttribute("isUnique","0");
                                            indexFile.setAttribute("indexType","BTree");
                                            indexFile.setAttribute("indexName",indexName+".ind");
                                            Element indexAttributes=doc.createElement("IndexAttributes");
                                            for (String f:fields
                                                    ) {
                                                Element iAttribute=doc.createElement("IAttribute");
                                                iAttribute.appendChild(doc.createTextNode(f));
                                                indexAttributes.appendChild(iAttribute);
                                            }
                                            indexFile.appendChild(indexAttributes);
                                            c.appendChild(indexFile);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File(url));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
