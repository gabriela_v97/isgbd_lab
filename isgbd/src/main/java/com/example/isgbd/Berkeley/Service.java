package com.example.isgbd.Berkeley;

import com.sleepycat.je.*;

import java.io.File;

public class Service {
    MyDbEnv databaseEnv;
    Database database;

    public Service(String databaseName){
        databaseEnv = new MyDbEnv(databaseName);
    }

    public void addRecord(String key, String value){
        try {
            databaseEnv.setup(new File("C:\\Facultate\\MASTER\\Semestrul I\\" +
                    "ISGBD\\isgbd\\isgbd\\src\\main\\resources\\database"), true);
            database = databaseEnv.getDatabase();
        } catch(DatabaseException dbe) {

        } finally {
            databaseEnv.close();
        }

        try {
            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
            DatabaseEntry theData = new DatabaseEntry(value.getBytes("UTF-8"));
            System.out.println(key + "-"+value);
            database.put(null, theKey, theData);
        } catch (Exception e) {

        }
    }

    public void readRecord(){
        String aKey = "1";

        try {
            // Create a pair of DatabaseEntry objects. theKey
            // is used to perform the search. theData is used
            // to store the data returned by the get() operation.
            DatabaseEntry theKey = new DatabaseEntry(aKey.getBytes("UTF-8"));
            DatabaseEntry theData = new DatabaseEntry();
            // Perform the get.
            if (database.get(null, theKey, theData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {

                // Recreate the data String.
                byte[] retData = theData.getData();
                String foundData = new String(retData, "UTF-8");
                System.out.println("For key: '" + aKey + "' found data: '" +
                        foundData + "'.");
            } else {
                System.out.println("No record found for key '" + aKey + "'.");
            }
        } catch (Exception e) {
            // Exception handling goes here
        }
    }



}
