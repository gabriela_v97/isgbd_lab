package com.example.isgbd.Berkeley;

import com.example.isgbd.DOMParser;
import com.sleepycat.je.*;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Service1 {
    MyDbEnv1 databaseEnv;
    List<Database> database;
    IndexService indexService;
   // Environment env;

    public Service1(){
        databaseEnv = new MyDbEnv1();
        try {
            databaseEnv.setup(new File("C:\\Facultate\\MASTER\\Semestrul I\\" +
                    "ISGBD\\isgbd\\isgbd\\src\\main\\resources\\database"), false);
            indexService = new IndexService();
           // database = databaseEnv.getDatabases();
        } catch(DatabaseException dbe) {

        } finally {
         //   databaseEnv.close();
        }
    }

    public String findByKey1(String databaseName, String table,
                            String key){
        List<String> list =  readAllRecords(databaseName, table);
        String[] elem;
        for(String value : list){
            elem = value.split("#");
            System.out.println("values = " + value);
            if(elem[0].equals(key)) {
                System.out.println("found = "+elem[0]);
                return value;
            }
        }
        return "notFound";
    }

    public String findByKey(String databaseName, String table,
                            String key){
        System.out.println("-------------- in find by key ------------------");
        List<String> list =  readAllRecords(databaseName, table);
        String[] elem;
        for(String value : list){
            elem = value.split("#");
            System.out.println("values = " + value);
            if(elem[0].equals(key)) {
                System.out.println("found = "+elem[0]);
                return "found";
            }
        }
        return "notFound";
    }

    public String addNewRecord(String databaseName, String table,
                             String key, String value, String FK, String UK, String NUK){
        System.out.println(" -------------- add new record -------------- ");
        Database myDatabase = databaseEnv.createDB(databaseName+"-"+table, false);
        System.out.println("key = " + key + "value = " +value);
        if(findByKey(databaseName, table, key).equals("found")){
            System.out.println("duplicateKey");
            return "duplicateKey";
        }else {
            try {
                System.out.println("not duplicateKey");
                DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
                DatabaseEntry theData = new DatabaseEntry(value.getBytes("UTF-8"));
                if(FK != null)
                    indexService.addIndexFK(databaseName, table, key, FK);
                if(NUK != null)
                    indexService.addIndexNonUK(databaseName, table, key, NUK);
                if(UK != null)
                    if(!indexService.addIndexUK(databaseName, table, key, UK).equals("duplicate")){
                       myDatabase.putNoOverwrite(null, theKey, theData);

                    }
                    else{
                        return "error: duplicate uk!";
                    }
                myDatabase.close();
                return "succes";
            } catch (Exception e) {
                System.out.println("eroare la adaugare!");
            }
        }
        return "error";
    }

    public void readRecords(String databaseName, String table, String key){
        Database myDatabase = databaseEnv.createDB(databaseName+"-"+table, false);
        try {
            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
            DatabaseEntry theData = new DatabaseEntry();

            if (myDatabase.get(null, theKey, theData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {

                byte[] retData = theData.getData();
                String foundData = new String(retData, "UTF-8");
                System.out.println("For key: '" + key + "' found data: '" +
                        foundData + "'.");
            } else {
                System.out.println("No record found for key '" + key + "'.");
            }
            myDatabase.close();
        } catch (Exception e) {
        }
    }


    public String deleteRecord(String databaseName, String table, String key){
        System.out.println("------------------ DELETE RECORD ------------------");
        Database myDatabase = databaseEnv.createDB(databaseName+"-"+table, false);
        String sem = "";
        System.out.println("table = " + table);
        try {
            DOMParser domParser = new DOMParser();
            IndexService indexService = new IndexService();
            List<String> tables = domParser.getTables(databaseName);
            for (String tableName : tables) {
                if (!tableName.equals(table)) {
                    String fk = domParser.getFK(databaseName, tableName);
                    System.out.println("fk values = " + fk);
                    if (!fk.equals("")) {
                        String[] fkValues = fk.split("#");
                        if (fkValues.length > 2) {
                            System.out.println("fkValues[2] = " + fkValues[2]);
                            if (fkValues[2].equals(table)) {
                                //se realizeaza stergerea doar daca nu exista acea valoare in copil
                                String FK = indexService.findFKByPK(databaseName, tableName, key);
                                System.out.println("FK value after search = " + FK);
                                if (FK.equals("")) {
                                    System.out.println("FK in equals = " + FK + ".");
                                    DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
                                    myDatabase.delete(null, theKey);
                                    myDatabase.close();
                                    updateFKIndexFile(databaseName, table, key);
                                    return "ok";
                                }else{
                                    sem = FK;
                                }

                            }
                        }
                    }
                }
            }
            if(sem.equals("")){
                try {
                    System.out.println("Nu e FK in niciun tabel");
                    DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
                    myDatabase.delete(null, theKey);
                    updateFKIndexFile(databaseName, table, key);
                    myDatabase.close();
                    return "ok";
                } catch (Exception ex) {

                }
            }

        } catch (Exception e) {
        }
        return "cannot delete";
    }

    public void updateFKIndexFile(String db, String table, String pk){
        IndexService indexService = new IndexService();
        Database myDatabase = databaseEnv.createDB("index" + "-"+  "FK" + "-" +
                db + "-" + table, false);
        List<String> fkValues = indexService.readAllRecordsFK(db,table);
        for(String fk : fkValues){
            String[] a = fk.split("#");
            for(int i = 1; i < a.length ; i++){
                if(a[i].equals(pk)){
                    //sterge pk din index
                    String key = a[0];
                    String newValue = "";
                    for(int j = 1 ; j < a.length ; j++){
                        if(!a[j].equals(pk))
                            newValue += a[j]+"#";
                    }
                    if(newValue.equals("")){
                        try {
                            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
                            myDatabase.delete(null, theKey);
                            myDatabase.close();
                        }catch (UnsupportedEncodingException ex){

                        }
                    }
                    else{
                        try {
                            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
                            myDatabase.delete(null, theKey);
                            DatabaseEntry key1 = new DatabaseEntry(key.getBytes("UTF-8"));
                            DatabaseEntry theData = new DatabaseEntry(newValue.getBytes("UTF-8"));
                            myDatabase.put(null, key1, theData);
                            myDatabase.close();
                        }catch (UnsupportedEncodingException ex){

                        }
                    }
                }
            }
        }

        System.out.println("--------- after update -----------");
        indexService.readRecords(db, table, "FK");
    }

    public List<String> readAllRecords(String databaseName, String table){
    //    System.out.println("----------------- Show rec: " + table);
        System.out.println("  -------------------------   READ AFTER ADD       --------------");
        Database myDatabase = databaseEnv.createDB(databaseName+"-"+table, false);
        List<String> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = myDatabase.openCursor(null, null);
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();
            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                String dataString = new String(foundData.getData());
                list.add(keyString+"#"+dataString);
                System.out.println("val from list : " + list.get(list.size()-1));
                System.out.println("Key : Data : " + keyString + " : " +
                        dataString + "");
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } finally {
            cursor.close();
        }
        return list;
    }

    public List<String> readAllKeys(String databaseName, String table){
        Database myDatabase = databaseEnv.createDB(databaseName+"-"+table, false);
        List<String> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = myDatabase.openCursor(null, null);
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();
            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                list.add(keyString);
                System.out.println("Key : " + keyString);
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } finally {
            cursor.close();
        }
        return list;
    }

}
