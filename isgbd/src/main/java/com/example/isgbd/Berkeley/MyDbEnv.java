package com.example.isgbd.Berkeley;

import com.sleepycat.je.*;
import com.sleepycat.je.DatabaseException;

import java.io.File;
import java.util.ArrayList;

public class MyDbEnv {
    private Environment myEnv;
    private Database database;
    private String databaseName;
    private ArrayList<Database> databases;

    public MyDbEnv(String databaseName) {
        this.databaseName = databaseName;
    }

    public void setup(File envHome, boolean readOnly)
            throws DatabaseException {

        // Instantiate an environment and database configuration object
        EnvironmentConfig myEnvConfig = new EnvironmentConfig();
        DatabaseConfig myDbConfig = new DatabaseConfig();
        // Configure the environment and databases for the read-only
        // state as identified by the readOnly parameter on this
        // method call.
        myEnvConfig.setReadOnly(readOnly);
        myDbConfig.setReadOnly(readOnly);
        // If the environment is opened for write, then we want to be
        // able to create the environment and databases if
        // they do not exist.
        myEnvConfig.setAllowCreate(!readOnly);
        myDbConfig.setAllowCreate(!readOnly);

        // Instantiate the Environment. This opens it and also
        // creates it.
        myEnv = new Environment(envHome, myEnvConfig);

        // Now create and open our databases.
        database = myEnv.openDatabase(null,
                databaseName,
                myDbConfig);
    }

    // Getter methods
    public Environment getEnvironment() {
        return myEnv;
    }

    public Database getDatabase() {
        return database;
    }

    // Close the environment
    public void close() {
        if (myEnv != null) {
            try {
                database.close();
                myEnv.close();
            } catch(DatabaseException dbe) {
                System.err.println("Error closing MyDbEnv: " +
                        dbe.toString());
                System.exit(-1);
            }
        }
    }
}
