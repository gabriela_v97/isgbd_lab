package com.example.isgbd.Berkeley;

import com.sleepycat.je.*;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;

public class IndexService {
    MyDbEnv1 databaseEnv;
    // Environment env;

    public IndexService(){
        databaseEnv = new MyDbEnv1();
        try {
            databaseEnv.setup(new File("C:\\Facultate\\MASTER\\Semestrul I\\" +
                    "ISGBD\\isgbd\\isgbd\\src\\main\\resources\\database"), false);
            // database = databaseEnv.getDatabases();
        } catch(DatabaseException dbe) {

        } finally {
            //   databaseEnv.close();
        }
    }

    public String addIndexUK(String databaseName, String table, String pk, String uk){
        Database myDatabase = databaseEnv.createDB("index" + "-"+ "UK"  + "-" +
                databaseName + "-" + table, false);
        Cursor cursor = null;
        try {
            cursor = myDatabase.openCursor(null, null);
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();
            String dataString = "";
            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                if (keyString.equals(uk)) {
                    return "duplicate";
                }
            }
                DatabaseEntry theKey = new DatabaseEntry(uk.getBytes("UTF-8"));
                DatabaseEntry theData = new DatabaseEntry(pk.getBytes("UTF-8"));
                myDatabase.putNoOverwrite(null, theKey, theData);
                return "ok";
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } catch (UnsupportedEncodingException e){

        }
        finally {
            cursor.close();
        }
        myDatabase.close();
        return "not";
    }

    public void addIndexNonUK(String databaseName, String table, String pk, String uk){
        Database myDatabase = databaseEnv.createDB("index" + "-"+ "NUK"  + "-" +
                databaseName + "-" + table, false);
        Cursor cursor = null;
        try {
            boolean sem = true;
            cursor = myDatabase.openCursor(null, null);
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();
            String dataString = "";
            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                //   System.out.println(dataString);
                String keyString = new String(foundKey.getData());
                // System.out.println(uk + "="+keyString);
                if (keyString.equals(uk)) {
                    dataString = new String(foundData.getData());
                    dataString += "#" + pk;
                    // System.out.println("dataString = " +dataString);
                    sem = false;
                }
            }
            //  System.out.println("addIndexFK " + dataString);
            if(sem == false) {
                // System.out.println("in false");
                DatabaseEntry theKey = new DatabaseEntry(uk.getBytes("UTF-8"));
                DatabaseEntry theData = new DatabaseEntry(dataString.getBytes("UTF-8"));
                myDatabase.put(null, theKey, theData);
            }
            else {
                // System.out.println("in true");
                DatabaseEntry theKey = new DatabaseEntry(uk.getBytes("UTF-8"));
                DatabaseEntry theData = new DatabaseEntry(pk.getBytes("UTF-8"));
                myDatabase.putNoOverwrite(null, theKey, theData);
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } catch (UnsupportedEncodingException e){

        }
        finally {
            cursor.close();
        }
        myDatabase.close();
    }

    //avem: key = index1 ; value = PK1#PK2...
    public void addIndexFK(String databaseName, String table, String pk, String fk){
        Database myDatabase = databaseEnv.createDB("index" + "-"+ "FK" + "-"+
                databaseName + "-" + table, false);
        Cursor cursor = null;
        try {
            boolean sem = true;
            cursor = myDatabase.openCursor(null, null);
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();
            String dataString = "";
            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
               // System.out.println(dataString);
                String keyString = new String(foundKey.getData());
              //  System.out.println(fk + "="+keyString);
                if (keyString.equals(fk)) {
                    dataString = new String(foundData.getData());
                    dataString += "#" + pk;
                   // System.out.println("dataString = " +dataString);
                    sem = false;
                }
            }
                System.out.println("addIndexFK " + dataString);
                if(sem == false) {
                 //   System.out.println("in false");
                    DatabaseEntry theKey = new DatabaseEntry(fk.getBytes("UTF-8"));
                    DatabaseEntry theData = new DatabaseEntry(dataString.getBytes("UTF-8"));
                    myDatabase.put(null, theKey, theData);
                }
                else {
                  //  System.out.println("in true");
                    DatabaseEntry theKey = new DatabaseEntry(fk.getBytes("UTF-8"));
                    DatabaseEntry theData = new DatabaseEntry(pk.getBytes("UTF-8"));
                    myDatabase.putNoOverwrite(null, theKey, theData);
                }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } catch (UnsupportedEncodingException e){
        }
        finally {
            cursor.close();
        }
        myDatabase.close();
    }

    //avem: key = index1#index2... ; value = PK
    public void addNewIndexFK(String databaseName, String table){
        Database myDatabase = databaseEnv.createDB("index" + "-"+ "FK" + "-" +
                databaseName + "-" + table, false);
            //read all elements from table
        Database db = databaseEnv.createDB(databaseName+"-"+table, false);
        Cursor cursor = null;
       // System.out.println("1");
        try {
            // Open the cursor.
            cursor = db.openCursor(null, null);

            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                String dataString = new String(foundData.getData());
                //preiau FK
                String[] values = dataString.split("#");
                String FK = values[0];
                DatabaseEntry theKey = new DatabaseEntry(FK.getBytes("UTF-8"));
                DatabaseEntry theData = new DatabaseEntry(keyString.getBytes("UTF-8"));
                myDatabase.putNoOverwrite(null, theKey, theData);
               // System.out.println("Key - Data - FK  : " + keyString + " : " +
                //        dataString + "" + FK);
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } catch (UnsupportedEncodingException e){

        }
        finally {
            cursor.close();
        }
            myDatabase.close();
            db.close();
    }

       public void readRecords(String databaseName, String table, String index_type){
        //    System.out.println("index_type = "+index_type + "-");
        Database myDatabase = databaseEnv.createDB("index" + "-"+  index_type + "-" +
                databaseName + "-" + table, false);
        //  System.out.println("in read record = "+ "index" + "-"+ index_type + "-" +
        //         databaseName + "-" + table);
        Cursor cursor = null;
        try {
            cursor = myDatabase.openCursor(null, null);
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                String dataString = new String(foundData.getData());
                System.out.println(index_type + "  Key = " + keyString + " : " +
                        dataString + "");
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } finally {
            cursor.close();
            myDatabase.close();
        }
    }

    public List<String> readRecordsFK(String databaseName, String table, String index_type){
        Database myDatabase = databaseEnv.createDB("index" + "-"+  index_type + "-" +
                databaseName + "-" + table, false);
        List<String> list = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = myDatabase.openCursor(null, null);
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                String dataString = new String(foundData.getData());
                System.out.println("Key = " + keyString + " : " +
                        dataString + "");
                list.add(keyString + "#" +dataString);
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } finally {
            cursor.close();
            myDatabase.close();
        }
        return list;
    }

    //for FK
    public void findAllByFK(String databaseName, String table, String FK){
        Database myDatabase = databaseEnv.createDB("index" + "-"+ "FK" +
                databaseName + "-" + table, false);
        Cursor cursorIndex = null;
        List<String> keys = new ArrayList<>();
        try {
            cursorIndex = myDatabase.openCursor(null, null);

            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            while (cursorIndex.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                String dataString = new String(foundData.getData());
                System.out.println("1. am gasit in fisierul de index ---- Key : Data : " + keyString + " : " + dataString + "");
                if(keyString.equals(FK)) {
                   System.out.println("2. am gasit in fisierul de index ---- Key : Data : " + keyString + " : " + dataString + "");
                    keys.add(dataString);
                }
            }
            findInTableByFK(databaseName, table, keys);
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } finally {
            cursorIndex.close();
        }
   }

    private void findInTableByFK(String databaseName, String table, List<String> primary_keys) {
        System.out.println("---------------- Find by FK ----------------");
        Database db = databaseEnv.createDB(databaseName+"-"+table, false);
        Cursor cursor = null;
        try {
            cursor = db.openCursor(null, null);

            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                String keyString = new String(foundKey.getData());
                String dataString = new String(foundData.getData());
                System.out.println(" toate -> Key : Data : " + keyString + " : " + dataString + "");
                if(primary_keys.contains(keyString)) {
                      System.out.println("cu index -> Key : Data : " + keyString + " : " + dataString + "");
                }
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } finally {
            cursor.close();
            db.close();
        }
    }

    public List<String> getSelectResults(String database, String table,
                                         String columns, String field,
                                         String operator, String value){
        String[] s = field.split(":");
        String constraintType = s[0];
        List<String> results = new ArrayList<>();
        Service1 service = new Service1();
        field.replaceAll("\\s+","");
        constraintType.replaceAll("\\s+","");
        value.replaceAll("\\s+","");
        operator.replaceAll("\\s+","");
        System.out.println("columns="+columns+".");
        System.out.println("pk="+constraintType+".");
        System.out.println("value="+value+".");

        //PK
        if(constraintType.equals("PK") && columns.equals("*") && value.equals("")){
            return service.readAllRecords(database, table);
        }else{
            if(constraintType.equals("PK") && columns.equals("*") && !value.equals(""))
                return getFilterRecordsByPK(database, table, field, operator, value);
        }

        //FK
        if(constraintType.equals("FK") && columns.equals("*") && value.equals("")){
            return readAllRecordsFK(database, table);
        }else{
            if(constraintType.equals("FK") && columns.equals("*") && !value.equals(""))
                return getFilterRecordsByFK(database, table, field, operator, value);
        }

        //NUK
        if(constraintType.equals("NUK") && columns.equals("*") && value.equals("")){
            return readAllRecordsNUK(database, table);
        }else{
            if(constraintType.equals("NUK") && columns.equals("*") && !value.equals(""))
                return getFilterRecordsByNUK(database, table, field, operator, value);
        }

        //UK
        if(constraintType.equals("UK") && columns.equals("*") && value.equals("")){
            return readAllRecordsUK(database, table);
        }else{
            if(constraintType.equals("UK") && columns.equals("*") && !value.equals(""))
                return getFilterRecordsByUK(database, table, field, operator, value);
        }
        return results;
    }

    private List<String> getFilterRecordsByNUK(String database, String table, String field, String operator, String value) {
        Service1 service = new Service1();
        List<String> pkList = new ArrayList<>();
        for(String elem1 : readRecordsFK(database, table, "NUK")){
            String[] e = elem1.split("#");
            for(int i = 1; i < e.length; i++){
                String found = service.findByKey1(database, table, e[i]);
                if(!found.equals("notFound")){
                    if(operator.equals("==")){
                        if(e[0].equals(value))
                            pkList.add(found);
                    }
                }
                if(operator.equals("!=")){
                    if(!e[0].equals(value))
                        pkList.add(found);
                }
                if(operator.equals("<")){
                    if(value.compareTo(e[0]) > 0)
                        pkList.add(found);
                }
                if(operator.equals("<=")){
                    if(value.compareTo(e[0]) >= 0)
                        pkList.add(found);
                }
                if(operator.equals(">")){
                    if(value.compareTo(e[0]) < 0)
                        pkList.add(found);
                }
                if(operator.equals(">=")){
                    if(value.compareTo(e[0]) <= 0)
                        pkList.add(found);
                }
            }
        }
        return pkList;

    }

    private List<String> readAllRecordsNUK(String database, String table) {
        Service1 service = new Service1();
        List<String> newList = new ArrayList<>();
        for(String elem : readRecordsFK(database, table, "NUK")){
            //    System.out.println("elem ==== " + elem);
            String[] e = elem.split("#");
            for(int i = 1; i < e.length; i++){
                String found = service.findByKey1(database, table, e[i]);
                if(!found.equals("notFound")){
                    newList.add(found);
                }
            }
        }
        return newList;
    }

    public List<String> readAllRecordsFK(String database, String table) {
        Service1 service = new Service1();
        List<String> newList = new ArrayList<>();
        for(String elem : readRecordsFK(database, table, "FK")){
         //   System.out.println("elem ==== " + elem);
            String[] e = elem.split("#");
            for(int i = 1; i < e.length; i++){
                String found = service.findByKey1(database, table, e[i]);
                if(!found.equals("notFound")){
                    newList.add(found);
                }
            }
        }
        return newList;
    }

    private List<String> getFilterRecordsByFK(String database, String table, String field, String operator, String value) {
        Service1 service = new Service1();
        List<String> pkList = new ArrayList<>();
        for(String elem1 : readRecordsFK(database, table, "FK")){
            String[] e = elem1.split("#");
            for(int i = 1; i < e.length; i++){
                String found = service.findByKey1(database, table, e[i]);
                if(!found.equals("notFound")){
                    if(operator.equals("==")){
                            if(e[0].equals(value))
                                pkList.add(found);
                        }
                    }
                    if(operator.equals("!=")){
                        if(!e[0].equals(value))
                            pkList.add(found);
                    }
                    if(operator.equals("<")){
                            if(value.compareTo(e[0]) > 0)
                                pkList.add(found);
                        }
                    if(operator.equals("<=")){
                            if(value.compareTo(e[0]) >= 0)
                                pkList.add(found);
                    }
                    if(operator.equals(">")){
                            if(value.compareTo(e[0]) < 0)
                                pkList.add(found);
                    }
                    if(operator.equals(">=")){
                            if(value.compareTo(e[0]) <= 0)
                                pkList.add(found);
                    }
                }
            }
        return pkList;
    }

    private List<String> getFilterRecordsByPK(String database, String table, String field, String operator, String value) {
        Service1 service = new Service1();
        List<String> list = service.readAllRecords(database, table);
        System.out.println(list.size());
        List<String> newList = new ArrayList<>();
        if(operator.equals("==")){
            for(String elem : list){
                String[] e = elem.split("#");
                if(e[0].equals(value))
                    newList.add(elem);
            }
        }
        if(operator.equals("!=")){
            for(String elem : list){
                String[] e = elem.split("#");
                if(!e[0].equals(value))
                    newList.add(elem);
            }
        }
        if(operator.equals("<")){
            for(String elem : list){
                String[] e = elem.split("#");
                if(value.compareTo(e[0]) > 0)
                    newList.add(elem);
            }
        }
        if(operator.equals("=<")){
            for(String elem : list){
                String[] e = elem.split("#");
                if(value.compareTo(e[0]) >= 0)
                    newList.add(elem);
            }
        }
        if(operator.equals(">")){
            for(String elem : list){
                String[] e = elem.split("#");
                if(value.compareTo(e[0]) < 0)
                    newList.add(elem);
            }
        }
        if(operator.equals(">=")){
            for(String elem : list){
                String[] e = elem.split("#");
                if(value.compareTo(e[0]) <= 0)
                    newList.add(elem);
            }
        }
        return newList;
    }

    private List<String> readAllRecordsUK(String database, String table) {
        Service1 service = new Service1();
        List<String> newList = new ArrayList<>();
        for(String elem : readRecordsFK(database, table, "UK")){
            String[] e = elem.split("#");
            for(int i = 1; i < e.length; i++){
                String found = service.findByKey1(database, table, e[i]);
                if(!found.equals("notFound")){
                    newList.add(found);
                }
            }
        }
        return newList;
    }


    public String findFKByPK(String database, String table,
                             String pk){
        String FK = "";
        List<String> list = readRecordsFK(database, table,"FK");
        System.out.println("table  from findFKByPK = "+table);
        System.out.println("list = " +list.size());
        for(String s : list){
            String[] e = s.split("#");
            System.out.println("list element = " + s);
            for(int i = 1; i < e.length; i++){
                if(e[i].equals(pk)){
                    System.out.println("in findFKByPK : " + e[i]);
                    return e[i];

                }

            }
        }
        return FK;
    }

    private List<String> getFilterRecordsByUK(String database, String table, String field, String operator, String value) {
        Service1 service = new Service1();
        List<String> pkList = new ArrayList<>();
        for(String elem1 : readRecordsFK(database, table, "UK")){
            String[] e = elem1.split("#");
            for(int i = 1; i < e.length; i++){
                String found = service.findByKey1(database, table, e[i]);
                if(!found.equals("notFound")){
                    if(operator.equals("==")){
                        if(e[0].equals(value))
                            pkList.add(found);
                    }
                }
                if(operator.equals("!=")){
                    if(!e[0].equals(value))
                        pkList.add(found);
                }
                if(operator.equals("<")){
                    if(value.compareTo(e[0]) > 0)
                        pkList.add(found);
                }
                if(operator.equals("<=")){
                    if(value.compareTo(e[0]) >= 0)
                        pkList.add(found);
                }
                if(operator.equals(">")){
                    if(value.compareTo(e[0]) < 0)
                        pkList.add(found);
                }
                if(operator.equals(">=")){
                    if(value.compareTo(e[0]) <= 0)
                        pkList.add(found);
                }
            }
        }
        return pkList;
    }

    public List<String> getJoinResults(String database, String table1, String table2,
                                       String column1, String column2, String relation){

        System.out.println("---------------- JOIN ---------------");
        Service1 service = new Service1();
        List<String> results = new ArrayList<>();
        //tabela parinte
        List<String> table1Values = service.readAllKeys(database, table1);
        //iau fks din tabela copil
        List<String> table2Values = readRecordsFK(database,table2,"FK");
        String result = "";

        for(String value : table2Values){
            System.out.println("values from fk : " + value);
            String[] values = value.split("#");
            String foreignKey = values[0];
            System.out.println("value from fk : " + foreignKey);
            for(String primaryKey : table1Values){
                System.out.println("value from t1 : " + primaryKey);
                if(relation.equals("==") && foreignKey.equals(primaryKey)){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += service.findByKey1(database, table1, primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals("!=") && !foreignKey.equals(primaryKey)){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += service.findByKey1(database, table1, primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals("<=") && foreignKey.compareTo(primaryKey) >= 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += service.findByKey1(database, table1, primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals(">=") && foreignKey.compareTo(primaryKey) <= 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += service.findByKey1(database, table1, primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals(">") && foreignKey.compareTo(primaryKey) < 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += service.findByKey1(database, table1, primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals("<") && foreignKey.compareTo(primaryKey) > 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += service.findByKey1(database, table1, primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
            }
        }
        return results;
    }

    public List<String> getHahsJoinResults(String database, String table1, String table2,
                                       String column1, String column2, String relation){

        System.out.println("---------------- HASH JOIN ---------------");
        Service1 service = new Service1();
        List<String> results = new ArrayList<>();
        List<String> table1Values = service.readAllRecords(database, table1);
        HashMap<String, String> table1ValuesDictionary = new HashMap<>();
        for(String element : table1Values) {
            String[] elements = element.split("#");
            table1ValuesDictionary.put(elements[0], elements[1]);
        }

        List<String> table2Values = readRecordsFK(database,table2,"FK");
        String result = "";

        for(String value : table2Values){
            System.out.println("values from fk : " + value);
            String[] values = value.split("#");
            String foreignKey = values[0];
            System.out.println("value from fk : " + foreignKey);
            for(String primaryKey : table1ValuesDictionary.keySet()){
                System.out.println("pk value from t1 : " + primaryKey);
                if(relation.equals("==") && foreignKey.equals(primaryKey)){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += table1ValuesDictionary.get(primaryKey); //service.findByKey1(database, table1, primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals("!=") && !foreignKey.equals(primaryKey)){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += table1ValuesDictionary.get(primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals("<=") && foreignKey.compareTo(primaryKey) >= 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += table1ValuesDictionary.get(primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals(">=") && foreignKey.compareTo(primaryKey) <= 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += table1ValuesDictionary.get(primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals(">") && foreignKey.compareTo(primaryKey) < 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += table1ValuesDictionary.get(primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
                if(relation.equals("<") && foreignKey.compareTo(primaryKey) > 0){
                    //iau valoarea pentru tabelul 2
                    String[] primaryKeysTable2 = values;
                    for(int i = 1 ; i < primaryKeysTable2.length ; i++){
                        //am luat valoarea pt tabelul 1
                        result += table1ValuesDictionary.get(primaryKey);
                        String pk = primaryKeysTable2[i];
                        String found = service.findByKey1(database, table2, pk);
                        System.out.println("found in table 2 : " + found);
                        if(!found.equals("notFound")){
                            result += "-";
                            result += found;
                            System.out.println("result of join : " + result);
                            results.add(result);
                        }
                        result = "";
                    }
                }
            }
        }
        return results;
    }

    public List<String> getGroupByResults(String database, String table,
                                          String aggregate, String columnAggregate,
                                          String columnGroupBy, String operator,
                                          String value){
        List<String> list = new ArrayList<>();
        String[] a = columnAggregate.split(":");
        String aggregateType = a[0];
        a = columnGroupBy.split(":");
        String groupByType = a[0];
        System.out.println("aggregateType = " + aggregateType);
        System.out.println("groupByType = " + groupByType);
        //if column to aggregate is PK:
        if(aggregateType.equals("PK")){
            //works only with index file for groupByType
            List<String> indexList= readRecordsFK(database, table, groupByType);
            for(String element : indexList){
                System.out.println("indexList element = " + element);
                //remove duplicate keys
                String[] elements = element.split("#");
                String key = elements[0];
                if(aggregate.equals("SUM")){
                    int sum = 0;
                    for(int i = 1 ; i < elements.length; i++){
                        sum += Integer.parseInt(elements[i]);
                    }
                    if(operator.equals("==") && sum == Integer.parseInt(value))
                            list.add(key + "#" + sum);
                    if(operator.equals("!=") && sum != Integer.parseInt(value))
                        list.add(key + "#" + sum);
                    if(operator.equals(">") && sum > Integer.parseInt(value))
                        list.add(key + "#" + sum);
                    if(operator.equals("<") && sum < Integer.parseInt(value))
                        list.add(key + "#" + sum);
                    if(operator.equals("<=") && sum <= Integer.parseInt(value))
                        list.add(key + "#" + sum);
                    if(operator.equals(">=") && sum >= Integer.parseInt(value))
                        list.add(key + "#" + sum);
                }
                if(aggregate.equals("AVG")){
                    int sum = 0;
                    for(int i = 1 ; i < elements.length; i++){
                        sum += Integer.parseInt(elements[i]);
                    }
                    System.out.println("AVG = " + (float)sum/(elements.length-1));
                    if(operator.equals("==") && (float)sum/(elements.length-1) == Float.parseFloat(value))
                        list.add(key + "#" + (float)sum/(elements.length-1));
                    if(operator.equals("!=") && (float)sum/(elements.length-1) != Float.parseFloat(value))
                        list.add(key + "#" + (float)sum/(elements.length-1));
                    if(operator.equals(">") && (float)sum/(elements.length-1) > Float.parseFloat(value))
                        list.add(key + "#" + (float)sum/(elements.length-1));
                    if(operator.equals("<") && (float)sum/(elements.length-1) < Float.parseFloat(value))
                        list.add(key + "#" + (float)sum/(elements.length-1));
                    if(operator.equals("<=") && (float)sum/(elements.length-1) <= Float.parseFloat(value))
                        list.add(key + "#" + (float)sum/(elements.length-1));
                    if(operator.equals(">=") && (float)sum/(elements.length-1) >= Float.parseFloat(value))
                        list.add(key + "#" + (float)sum/(elements.length-1));
                }
                if(aggregate.equals("COUNT")){
                    int count = elements.length - 1;
                    if(operator.equals("==") && count == Integer.parseInt(value))
                        list.add(key + "#" + count);
                    if(operator.equals("!=") && count != Integer.parseInt(value))
                        list.add(key + "#" + count);
                    if(operator.equals(">") && count > Integer.parseInt(value))
                        list.add(key + "#" + count);
                    if(operator.equals("<") && count < Integer.parseInt(value))
                        list.add(key + "#" + count);
                    if(operator.equals("<=") && count <= Integer.parseInt(value))
                        list.add(key + "#" + count);
                    if(operator.equals(">=") && count >= Integer.parseInt(value))
                        list.add(key + "#" + count);
                }
                if(aggregate.equals("MAX")){
                    int max = 0;
                    for(int i = 1 ; i < elements.length; i++){
                        if(max < Integer.parseInt(elements[i]))
                            max = Integer.parseInt(elements[i]);
                    }
                    if(operator.equals("==") && max == Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals("!=") && max != Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals(">") && max > Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals("<") && max < Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals("<=") && max <= Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals(">=") && max >= Integer.parseInt(value))
                        list.add(key + "#" + max);
                }
                if(aggregate.equals("MIN")){
                    int max = 30000;
                    for(int i = 1 ; i < elements.length; i++){
                        if(max > Integer.parseInt(elements[i]))
                        max = Integer.parseInt(elements[i]);
                    }
                    System.out.println("MIN = " + key + " : " + max);
                    if(operator.equals("==") && max == Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals("!=") && max != Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals(">") && max > Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals("<") && max < Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals("<=") && max <= Integer.parseInt(value))
                        list.add(key + "#" + max);
                    if(operator.equals(">=") && max >= Integer.parseInt(value))
                        list.add(key + "#" + max);
                }
            }
        }
        else{
            if(aggregateType.equals("UK")){
                list = new ArrayList<>();
                List<String> indexListGroup= readRecordsFK(database, table, groupByType);
                List<String> indexListAggregate= readRecordsFK(database, table, "UK");
                //create a hashmap with key = groupByType and value = list of primary keys
                HashMap<String, List<String>> hashMapGroupByType = new HashMap<>();
                //create a hashmap with key = PK and value = UK
                HashMap<String, String> hashMapPKAndUK = new HashMap<>();

                //populate hashMapGroupByType
                for(String s : indexListGroup){
                    String[] ss = s.split("#");
                    List<String> keys = new ArrayList<>();
                    for(int i = 1; i < ss.length; i++)
                        keys.add(ss[i]);
                    hashMapGroupByType.put(ss[0], keys);
                    System.out.println("populate hashMapGroupByType : " + s);
                }

                //populate hashMapPKAndUK
                for(String s : indexListAggregate){
                    String[] ss = s.split("#");
                    hashMapPKAndUK.put(ss[1], ss[0]);
                    System.out.println("populate hashMapPKAndUK : " + s);
                }

                for(String groupKey : hashMapGroupByType.keySet()){
                    if(aggregate.equals("COUNT")){
                        int count = 0;
                        //iterate all PK from groupType hash
                        for(String pkGroup : hashMapGroupByType.get(groupKey)){
                            System.out.println("pkGroup = " + pkGroup);
                            //find pk in hashMapPKAndUK
                            for(String pk : hashMapPKAndUK.keySet()){
                                System.out.println("pk = " + pk);
                                if(pk.equals(pkGroup)){
                                    count ++;
                                }
                            }

                            if(operator.equals("==") && count == Integer.parseInt(value))
                                list.add(groupKey + "#" + count);
                            if(operator.equals("!=") && count != Integer.parseInt(value))
                                list.add(groupKey + "#" + count);
                            if(operator.equals(">") && count > Integer.parseInt(value))
                                list.add(groupKey + "#" + count);
                            if(operator.equals("<") && count < Integer.parseInt(value))
                                list.add(groupKey + "#" + count);
                            if(operator.equals("<=") && count <= Integer.parseInt(value))
                                list.add(groupKey + "#" + count);
                            if(operator.equals(">=") && count >= Integer.parseInt(value))
                                list.add(groupKey + "#" + count);

                        }
                    }
                }
            }
            int sem = 0;
            for(int i = 0; i< list.size(); i++){
                String[] split1 = list.get(i).split("#");
                sem = 0;
                for(int j = 0; j < list.size();j++){
                    String[] split2 = list.get(j).split("#");
                    if(split1[0].equals(split2[0]) && i!=j)
                        sem = 1;
                }
                if(sem == 1){
                    list.remove(i);
                }
            }
        }


        return list;
    }
}
