package com.example.isgbd.Berkeley;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Database;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.je.Environment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyDbEnv1 {

    private Environment myEnv;
    private Database database;
    private List<Database> databaseList;

    public MyDbEnv1() {}

    public void setup(File envHome, boolean readOnly)
            throws DatabaseException {

        EnvironmentConfig myEnvConfig = new EnvironmentConfig();
        myEnvConfig.setReadOnly(readOnly);
        myEnvConfig.setAllowCreate(!readOnly);
        myEnv = new Environment(envHome, myEnvConfig);

        databaseList = new ArrayList<>();
    }

    public Database createDB(String databaseName, boolean readOnly){
        DatabaseConfig myDbConfig = new DatabaseConfig();
        myDbConfig.setReadOnly(readOnly);
        myDbConfig.setAllowCreate(!readOnly);
        database = myEnv.openDatabase(null, databaseName, myDbConfig);
        databaseList.add(database);
        return database;
    }

    public Environment getEnvironment() {
        return myEnv;
    }

    public List<Database> getDatabases() {
        return databaseList;
    }

    public void close() {
        if (myEnv != null) {
            try {
                for(Database db : databaseList)
                    db.close();
                myEnv.close();
            } catch(DatabaseException dbe) {
                System.err.println("Error closing MyDbEnv: " +
                        dbe.toString());
                System.exit(-1);
            }
        }
    }
}
